// General configs
exports.General = {
  buildingFloatingPosition: {
    x: 256,
    y: 152,
    floatDistance: 10,
  },
  leastAngleForMatching: Math.PI / 3,
  //leastAngleForMatching: Math.PI * 2,
  pointsPerDrop: 1,
  pointsPerBigDrop: 5,
  pointsPerEvolve: 3,
  pointsPerFusion: 2,   
  pointsPerEscape: 10,
};

// The planet and background
exports.Planet = {
  position: {
    x: 256,
    y: 750,
  },
  size : {
    width: 384,
    height: 384,
  },
  planetRadius: 185,
  backgroundSize : {
    width: 1024,
    height: 1024,
  },
  terrainSize : {
    width: 512,
    height: 512,
  },
  backgroundImage: "resources/images/planet/game_background.jpg",
  terrainImage: "resources/images/planet/planet_terrain.png",
  haloImage: "resources/images/planet/planet_halo.png",      
};

// Buildings to drop
exports.Buildings = {
  
  // Line 1
  plant: {
    evolve_to: "hut",
    width: 64,
    height: 64,
    collisionSize: 45,
    imagePath: "resources/images/buildings",
    image: ["tree_1_0.png", "tree_1_1.png", "tree_1_2.png", "tree_1_3.png", "tree_1_4.png"],
  },  
  hut: {
    evolve_to: "school",
    width: 64,
    height: 64,
    collisionSize: 48,
    imagePath: "resources/images/buildings",
    image: ["hut.png"],
  },  
  school: {
    evolve_to: "theatre",
    width: 72,
    height: 72,
    collisionSize: 54,
    imagePath: "resources/images/buildings",
    image: ["school.png"],
  },  
  theatre: {
    width: 112,
    height: 112,
    collisionSize: 84,
    imagePath: "resources/images/buildings",
    image: ["theatre.png"],
    special: "flying",
  },
  
  // Line 2
  tent: {
    evolve_to: "cottage",
    width: 64,
    height: 64,
    collisionSize: 45,
    imagePath: "resources/images/buildings",
    image: ["tent1.png", "tent2.png", "tent3.png"],
  },    
  cottage: {
    evolve_to: "apartments",
    width: 64,
    height: 64,
    collisionSize: 48,
    imagePath: "resources/images/buildings",
    image: ["cottage.png"],
  },
  apartments: {
    evolve_to: "pyramid",
    width: 72,
    height: 72,
    collisionSize: 54,
    imagePath: "resources/images/buildings",
    image: ["apartments.png"],
  },
  pyramid: {
    width: 112,
    height: 112,
    collisionSize: 84,
    imagePath: "resources/images/buildings",
    image: ["pyramid.png"],
    special: "flying",
  },
  
  // Line 3
  windmill: {
    evolve_to: "smithy",
    width: 64,
    height: 64,
    collisionSize: 48,
    imagePath: "resources/images/buildings",
    image: ["windmill.png"],
  },  
  smithy: {
    evolve_to: "factory",
    width: 64,
    height: 64,
    collisionSize: 48,
    imagePath: "resources/images/buildings",
    image: ["smithy.png"],
  },
  factory: {
    evolve_to: "church",
    width: 72,
    height: 72,
    collisionSize: 56,
    imagePath: "resources/images/buildings",
    image: ["factory.png"],
  },  
  church: {
    width: 112,
    height: 112,
    collisionSize: 84,
    imagePath: "resources/images/buildings",
    image: ["church.png"],
    special: "flying",
  },
  
  // Unused
  statue: {
    evolve_to: "church",
    width: 60,
    height: 60,
    collisionSize: 42,
    imagePath: "resources/images/buildings",
    image: ["statue.png"],
  },    
  
  // Tools
  bomb: {
    width: 52,
    height: 52,
    collisionSize: 52,
    imagePath: "resources/images/buildings",
    image: ["bomb.png"],
    special: 'demolish',
  },
  
  hammer: {
    width: 52,
    height: 52,
    collisionSize: 52,
    imagePath: "resources/images/buildings",
    image: ["hammer.png"],
    special: 'upgrade',
  },
  
  ufo: {
    width: 96,
    height: 96,
    collisionSize: 12,
    imagePath: "resources/images/buildings",
    image: ["ufo.png"],
    special: 'pull',
  },
};
