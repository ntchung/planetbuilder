import admob;
import animate;
import device;
import ui.TextView;
import ui.StackView as StackView;

import src.LevelData as LevelData;
import src.Game.PlayerProfile as PlayerProfile;
import src.Game.Sound as Sound;
import src.Screen.Logo as Logo;
import src.Screen.Splash as Splash;
import src.Screen.GamePlay as GamePlay;
import src.Utils.Data as Data;

exports = Class(GC.Application, function () {

    this.initUI = function () {

        // Base resolution is 512x1024
        var scaleRatio = device.width / 512;
        var rootView = new StackView({
            superview: this,
            x: 0,
            y: 0,
            width: 512,
            height: device.screen.height / scaleRatio,
            clip: true,
            scale: scaleRatio,
        });

        // Global information
        GLOBAL.screenWidth = rootView.style.width;
        GLOBAL.screenHeight = rootView.style.height;
        GLOBAL.levelsCount = Object.keys(LevelData).length;
        
        admob.showAdView({
            adUnitId: "ca-app-pub-8112894826901791/3036535668",
            adSize: "smart_banner",
            horizontalAlign: "center",
            verticalAlign: "bottom",
            reload: true,
        });

        PlayerProfile.load();
        Sound.init();

        // Screens and transitions
        var logo = new Logo();
        var splash = new Splash();
        var gamePlay = new GamePlay();

        var quickStart = false;

        if (!quickStart) {                        
            rootView.push(logo);
        } else {
            rootView.push(gamePlay);
            gamePlay.emit('app:start');
        }

        logo.on('go', function () {
            Sound.playRandomMusic();
            rootView.push(splash);            

        });

        splash.on('splash:start', function () {
            rootView.push(gamePlay);
            gamePlay.emit('app:start');
        });

        gamePlay.on('gameplay:home', function () {
            admob.showAdView({
                adUnitId: "ca-app-pub-8112894826901791/3036535668",
                adSize: "smart_banner",
                horizontalAlign: "center",
                verticalAlign: "bottom",
                reload: true,
            });
            Sound.playRandomMusic();
            splash.generateStars();
            rootView.pop();

            if (!rootView.getCurrentView()) {
                rootView.push(splash);
            }
        });
    };

});
