exports.English = {
    Title: "Planet Builder",
    Level: "Level",
    Score: "Score",
    BestScore: "Best score",
};

this.get = function(key) {
  return exports.English[key];
};
