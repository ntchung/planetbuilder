import animate;
import ui.View;
import ui.TextView as TextView;
import ui.ImageScaleView;
import ui.ImageView as ImageView;
import ui.resource.loader as loader;
import ui.widget.ButtonView as ButtonView;

import src.Game.BackgroundStar as BackgroundStar;
import src.Text as Text;
import src.Utils.Polar as Polar;

exports = Class(ui.ImageScaleView, function(supr) {
    
  this.init = function(opts)
  {
    opts = merge(opts, {
        scaleMethod: 'cover',
        x: 0,
        y: 0,
        width: GLOBAL.screenWidth,
        height: GLOBAL.screenHeight,
        image: "resources/images/planet/game_background.jpg",
    });

    supr(this, 'init', [opts]); 
        
    this._stars = [];
    this.generateStars();
    
    var logo = new ImageView({
      superview: this,
      x: GLOBAL.screenWidth / 2 - 200,
      y: 40,
      width: 400,
      height: 154,
      image: "resources/images/title/title_logo.png",
    });
    
    var decal = new ImageView({
      superview: this,
      x: GLOBAL.screenWidth / 2 - 240,
      y: GLOBAL.screenHeight / 2 - 210,
      anchorX: 240,
      anchorY: 240,
      width: 480,
      height: 480,
      image: "resources/images/title/title_decal.png",
    });    

    var view = this;

    // Show buttons
    var startButton = new ButtonView({
        superview: this,
        x: (GLOBAL.screenWidth / 2) - 80,
        y: (GLOBAL.screenHeight) - 250,
        width: 160,
        height: 160,
        images: {
            up: "resources/images/ui/big_button_play.png",
            down: "resources/images/ui/big_button_play_pressed.png"
        },        
        on: {
          up: function() {                    
            view.emit('splash:start');
          }
        }
    });      
    
  };   
  
  this.generateStars = function() {
    var w = GLOBAL.screenWidth - 48;
    var n = (w * 154) + Math.random() * w * 128;
    for (var i = 0; i < 10; ++i) {
      var x = n % w + 24;
      var y = n / w; 
      
      if (i >= this._stars.length) {
        var star = new BackgroundStar({
            superview: this,
            x: x,
            y: y,
          });
        this._stars.push(star);
      } 
      else {
        var star = this._stars[i];
        star.style.x = x;
        star.style.y = y;
      }
      
      n += w * 16 + Math.random() * w * 128;
    }    
  }
});
