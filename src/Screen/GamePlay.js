import ui.View;
import ui.TextView as TextView;
import ui.ImageScaleView;
import ui.ImageView as ImageView;
import ui.widget.ButtonView as ButtonView;
import ui.ParticleEngine as ParticleEngine;

import src.Config as Config;
import src.Utils.Polar as Polar;

import src.LevelData as LevelData;
import src.Game.PlayerProfile as PlayerProfile;
import src.Game.Sound as Sound;
import src.Game.Effects as Effects;
import src.Game.Planet as Planet;
import src.Game.BuildingManager as BuildingManager;
import src.Game.Building as Building;
import .InGameMenu as InGameMenu;
import .HudView as HudView;
import .WinView as WinView;

var GameState = {
  Loading: 0,
  Playing: 1,
  GameOver: 2,
  Pause: 3,
};

exports = Class(ui.View, function(supr) {
  this.init = function(opts) {        
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
    });
    
    supr(this, 'init', [opts]); 
        
    this._planet = new Planet({
      superview: this,      
      config: Config.Planet,      
    });
    
    this._buildingManager = new BuildingManager({
      superview: this,
      planet: this._planet,
      config: Config.Buildings,
    });
    
    this._particlesEngineLo = new ParticleEngine({
      superview: this,      
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
      centerAnchor: true
    });
    
    this._particlesEngineHi = new ParticleEngine({
      superview: this,      
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
      centerAnchor: true
    });    
    
    this._hudView = new HudView({
      superview: this,
    });
    
    this._igmView = new InGameMenu({
      superview: this,
      visible: false,
    });
    
    this._winView = new WinView({
      superview: this,
      visible: false,
    });
    
    this._particlesEngineTop = new ParticleEngine({
      superview: this,      
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
      centerAnchor: true
    });
    
    Effects.registerParticlesEngine([this._particlesEngineLo, this._particlesEngineHi, this._particlesEngineTop]);
    
    this._state = GameState.Loading;    
    this._currentScore = 0;    
    
    // Inputs handling                
    this.on('InputStart', function (event, point) {
      if (this._state == GameState.Playing && point.y > 96)
      {
        this._buildingManager._isDropping = true;
      }
    });
    
    // Events handling
    this.on('Lose', function() {      
      this._state = GameState.GameOver;                  
      
      PlayerProfile.announceScore(this._currentScore);
      PlayerProfile.save();
            
      this._winView.setScore(this._currentScore, PlayerProfile.getBestScore(), this.getStarsLevel());      
      this._winView.animateShow();            
    });
    
    this.on('app:start', function() {      
      this.restartLevel();
    });
            
    this.on('gameplay:retry', function() {
      this.restartLevel();
    });
    
    this.on('gameplay:pause', function() {
      this._state = GameState.Pause;
      this._buildingManager._isDropping = false;
      this._igmView.show();      
    });
    
    this.on('gameplay:restart', function() {
      this._state = GameState.Playing;
      this._buildingManager._isDropping = false;
      this._igmView.hide();
      
      this.restartLevel();
    });
    
    this.on('gameplay:resume', function() {
      this._state = GameState.Playing;
      this._buildingManager._isDropping = false;
      this._igmView.hide();
    });
  };
  
  this.getStarsLevel = function() {
    var cnt = GLOBAL.levelsCount;
    var lastLevel = LevelData[(cnt - 1).toString()];
    var progress = this._currentScore / lastLevel.score;    
    return Math.min(Math.floor(progress * 4), 3);
  };
  
  this.startLevel = function() {      
    this.cleanUp();
    
    this._currentScore = 0;
    this._hudView.setLevelScore(this._currentScore);
    
    Sound.playRandomMusic();
    
    this._planet.generateStars();
    this._planet.setInitialRotation(Math.random() * Math.PI * 2);
    
    this.setLevel(0);
    
    this._buildingManager.createNextBuilding();            
    this._state = GameState.Playing;    
  };
  
  this.setLevel = function(levelNum) {
    if (levelNum >= GLOBAL.levelsCount) {
      levelNum = GLOBAL.levelsCount - 1;
    }
    
    var level = LevelData[levelNum.toString()];
    this._currentLevel = levelNum;    
    this._planet.setRotateSpeed(level.rotation_speed);
    this._planet.setAcceleration(level.acceleration);
    this._planet.setReversal(level.laps_before_reverse);
    
    this._buildingManager.setLevel(level);
  };
  
  this.cleanUp = function() {
    this._buildingManager.cleanUp();
    
    this._winView.hide();
    this._igmView.hide();
  };

  this.tick = function(dt) {        
    this._buildingManager.update(dt);
    
    Effects.runParticlesEngine(dt);
    
    this._hudView.setLevelScore(this._currentScore);
    
    this.checkNextLevel();
    
    if (this._state == GameState.Playing) {
      this._planet.update(dt);
    }
  };  
  
  this.checkNextLevel = function() {
    var levelNum = this._currentLevel;
    if (levelNum < GLOBAL.levelsCount - 1) {      
      var level = LevelData[(levelNum + 1).toString()];
      
      if (this._currentScore >= level.score) {
        this.setLevel(levelNum + 1);
      }
    }
  };
  
  this.restartLevel = function() {    
    this._state = GameState.Loading;
    this._planet.reset();
    this.startLevel();
  };
  
  this.addScore = function(score) {
    this._currentScore += score;
  };
});
