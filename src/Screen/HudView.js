import ui.View;
import ui.TextView as TextView;
import ui.ImageScaleView;
import ui.ImageView as ImageView;
import ui.ScoreView as ScoreView;

import ui.widget.ButtonView as ButtonView;
import src.Game.NumberFont as NumberFont;
import src.Text as Text;

exports = Class(ui.View, function(supr) {
    
  this.init = function(opts)
  { 
    opts = merge(opts, {
      x: 0,  
      y: 0,
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
    });
    
    supr(this, 'init', [opts]);                
    
    var gameState = opts.superview;
    
    var bgScore = new ImageView({
      superview: this,
      x: 10,
      y: 10,
      width: 172,
      height: 64,
      image: "resources/images/ui/ig_score_box.png"
    });
    
    this._labelLevelScore = new ScoreView({
      superview: bgScore,
      x: 0,
      y: 25,            
      width: 160,
      height: 32,
      text: "0",
      horizontalAlign: "right",
      characterData: NumberFont.getScoreFontData(),            
    });
    
    this._labelBuildingsCount = new ScoreView({
      superview: this,
      x: GLOBAL.screenWidth / 2 - 150,
      y: 10,
      width: 300,
      height: 56,
      horizontalAlign: "center",
      characterData: NumberFont.getTimerFontData(),            
      text: "0",
    });
    
    var pauseButton = new ButtonView({
      superview: this,
      x: GLOBAL.screenWidth - 108,
      y: 18,
      width: 90,
      height: 90,
      images: {
          up: "resources/images/ui/button_pause.png",
          down: "resources/images/ui/button_pause_pressed.png"
      },
      on: {
        up: function() {
          gameState.emit('gameplay:pause');                    
        }
      }
    });
  };
  
  this.setLevelScore = function(score) {
    this._labelLevelScore.setText(score.toString());
  };
});
