import ui.View;
import ui.TextView as TextView;
import ui.ImageScaleView;
import ui.ImageView as ImageView;
import ui.resource.loader as loader;
import ui.widget.ButtonView as ButtonView;

import src.Text as Text;
import src.Utils.Polar as Polar;

exports = Class(ui.View, function(supr) {
    
  this.init = function(opts)
  {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
    });

    supr(this, 'init', [opts]);                

    var gameState = opts.superview;
    
    var blackOut = new ui.View({
      superview: this,
      x: 0,
      y: 0,
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
      backgroundColor: '#111111',
      opacity: 0.6,
    });
    
    // Panel
    var panel = new ImageView({
      superview: this,
      x: 20,
      y: GLOBAL.screenHeight / 2 - 100,
      width: GLOBAL.screenWidth - 40,
      height: 200,
      image: "resources/images/ui/igm_panel.png",
    });
    
    var ribbon = new ImageView({
      superview: this,
      x: GLOBAL.screenWidth / 2 - 104,
      y: GLOBAL.screenHeight / 2 - 130,
      width: 208,
      height: 60,
      image: "resources/images/ui/pause_ribbon.png",
    });

    // Show buttons
    var resumeButton = new ButtonView({
        superview: this,
        x: GLOBAL.screenWidth / 2 - 200,
        y: (GLOBAL.screenHeight / 2) - 60,
        width: 120,
        height: 120,
        images: {
            up: "resources/images/ui/button_play.png",
            down: "resources/images/ui/button_play_pressed.png"
        },        
        on: {
          up: function() {                    
            gameState.emit('gameplay:resume');
          }
        }
    });
    
    var homeButton = new ButtonView({
        superview: this,
        x: GLOBAL.screenWidth / 2 + 200 - 120,
        y: (GLOBAL.screenHeight / 2) - 60,
        width: 120,
        height: 120,
        images: {
            up: "resources/images/ui/button_home.png",
            down: "resources/images/ui/button_home_pressed.png"
        },        
        on: {
          up: function() {                    
            gameState.emit('gameplay:home');
          }
        }
    });
    
    var restartButton = new ButtonView({
        superview: this,
        x: GLOBAL.screenWidth / 2 - 60,
        y: (GLOBAL.screenHeight / 2) - 60,
        width: 120,
        height: 120,
        images: {
            up: "resources/images/ui/button_retry.png",
            down: "resources/images/ui/button_retry_pressed.png"
        },        
        on: {
          up: function() {                    
            gameState.emit('gameplay:restart');
          }
        }
    });
  };       
});
