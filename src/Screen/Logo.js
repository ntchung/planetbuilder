import animate;
import ui.View;
import ui.TextView as TextView;
import ui.ImageScaleView;
import ui.ImageView as ImageView;
import ui.resource.loader as loader;
import ui.widget.ButtonView as ButtonView;

import src.Text as Text;

exports = Class(ui.ImageScaleView, function(supr) {
    
  this.init = function(opts)
  {
    opts = merge(opts, {
        scaleMethod: 'cover',
        x: 0,
        y: 0,
        width: GLOBAL.screenWidth,
        height: GLOBAL.screenHeight,
        image: "resources/splash/splash.jpg",
    });
    
    supr(this, 'init', [opts]); 
    
    var d1 = new Date();
    var startLoadingTime = d1.getTime();
    
    var self = this;
    loader.preload(['resources/images', 'resources/fonts'], function() {
      var d2 = new Date();
      var finishLoadingTime = d2.getTime();
      
      var waitTime = 4000 - (finishLoadingTime - startLoadingTime);
      if (waitTime <= 0) {
        self.emit('go');
      }
      else {        
        animate(self).wait(waitTime)
          .then(function() { self.emit('go'); });
      }
    }); 
  }
});