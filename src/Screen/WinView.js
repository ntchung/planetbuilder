import animate;
import ui.View;
import ui.ImageScaleView;
import ui.ImageView as ImageView;
import ui.TextView as TextView;
import ui.widget.ButtonView as ButtonView;

import src.Game.Sound as Sound;
import src.Game.Effects as Effects;
import src.Text as Text;

exports = Class(ui.View, function(supr) {
    
  this.init = function(opts)
  { 
    opts = merge(opts, {
      x: 0,  
      y: 0,
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
    });
    
    supr(this, 'init', [opts]);                
    
    var gameState = opts.superview;
    
    this._blackOut = new ui.View({
      superview: this,
      x: 0,
      y: 0,
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
      backgroundColor: '#111111',
      opacity: 0.6,
    });
    
    // Panel
    var panel = new ImageView({
      superview: this,
      x: 20,
      y: GLOBAL.screenHeight / 2 - 250,
      anchorX: (GLOBAL.screenWidth - 40) / 2,
      anchorY: 250,
      width: GLOBAL.screenWidth - 40,
      height: 500,
      image: "resources/images/ui/level_cleared_panel.png",
    });
  
    this._panel = panel;
    
    var ribbon = new ImageView({
      superview: panel,
      x: panel.style.width / 2 - 212,
      y: -40,
      width: 424,
      height: 107,
      image: "resources/images/ui/level_cleared_ribbon.png",
    });
    
    var homeButton = new ButtonView({
      superview: panel,
      x: panel.style.width / 2 - 180,
      y: panel.style.height - 150,
      width: 120,
      height: 120,
      images: {
          up: "resources/images/ui/button_home.png",
          down: "resources/images/ui/button_home_pressed.png"
      },
      on: {
        up: function() {
          gameState.emit('gameplay:home');                    
        }
      }
    });
    
    var restartButton = new ButtonView({
      superview: panel,
      x: panel.style.width / 2 + 60,
      y: panel.style.height - 150,
      width: 120,
      height: 120,
      images: {
          up: "resources/images/ui/button_retry.png",
          down: "resources/images/ui/button_retry_pressed.png"
      },
      on: {
        up: function() {
          gameState.emit('gameplay:retry');                    
        }
      }
    });
        
    // Scores
    this._textBestScore = new TextView({
      superview: panel,
      fontFamily: 'CarterOne',
      x: 20,
      y: panel.style.height - 220,
      width: panel.style.width - 40,
      height: 42,
      horizontalAlign: "center",
      text: "Best score: 1000",
      size: 42,
      color: "white",
      shadowColor: '#111111',
      shadowWidth: 2
    });
    
    this._textScore = new TextView({
      superview: panel,
      fontFamily: 'CarterOne',
      x: 20,
      y: panel.style.height - 270,
      width: panel.style.width - 40,
      height: 42,
      horizontalAlign: "center",
      text: "Score: 1000",
      size: 42,
      color: "white",
      shadowColor: '#111111',
      shadowWidth: 2
    });
    
    // Stars
    var starBase1 = new ImageView({
      superview: panel,
      x: panel.style.width / 2 - 50 - 140,
      y: 120,
      r: -0.3,
      anchorX: 50,
      anchorY: 50,
      width: 100,
      height: 100,
      image: "resources/images/ui/star_base.png",
    });
    
    var starBase2 = new ImageView({
      superview: panel,
      x: panel.style.width / 2 - 75,
      y: 50,
      anchorX: 50,
      anchorY: 50,
      width: 150,
      height: 150,
      image: "resources/images/ui/star_base.png",
    });
    
    var starBase3 = new ImageView({
      superview: panel,
      x: panel.style.width / 2 - 50 + 140,
      y: 120,
      r: 0.3,
      anchorX: 50,
      anchorY: 50,
      width: 100,
      height: 100,
      image: "resources/images/ui/star_base.png",
    });
    
    var star1 = new ImageView({
      superview: panel,
      x: panel.style.width / 2 - 50 - 140,
      y: 120,
      r: -0.3,
      anchorX: 50,
      anchorY: 50,
      width: 100,
      height: 100,
      image: "resources/images/ui/star_on.png",
    });
    
    var star2 = new ImageView({
      superview: panel,
      x: panel.style.width / 2 - 75,
      y: 50,
      anchorX: 50,
      anchorY: 50,
      width: 150,
      height: 150,
      image: "resources/images/ui/star_on.png",
    });
    
    var star3 = new ImageView({
      superview: panel,
      x: panel.style.width / 2 - 50 + 140,
      y: 120,
      r: 0.3,
      anchorX: 50,
      anchorY: 50,
      width: 100,
      height: 100,
      image: "resources/images/ui/star_on.png",
    });
    
    this._stars = [];
    this._stars.push(star1);
    this._stars.push(star2);
    this._stars.push(star3);    
  };
  
  this.setScore = function(score, bestScore, gotStars) {
    this._textBestScore.setText(Text.get("BestScore") + ": " + bestScore.toString());
    this._textScore.setText(Text.get("Score") + ": " + score.toString());
    
    for (var i = this._stars.length - 1; i >= 0; --i) {
      if (i < gotStars) {
        this._stars[i].show();
      }
      else {
        this._stars[i].hide();
      }
    }    
  };
  
  this.animateShow = function() {
    this.show();
    
    var panel = this._panel;
    
    this._blackOut.style.opacity = 0.0;
    panel.style.scale = 0.0;    
    
    animate(this._blackOut)
      .now({opacity: 0.6}, 500, animate.linear);
    
    var x = panel.style.x + panel.style.width * 0.5;
    var y = panel.style.y + panel.style.height * 0.5;
    
    animate(panel)
      .wait(900)
      .then(function() {
        Effects.fireworksUI(x, y);
      }) 
      .wait(100)
      .then({scale: 1.2}, 250, animate.easeIn)
      .then({scale: 1.0}, 250, animate.easeOut);
    
    for (var i = this._stars.length - 1; i >= 0; --i) {
      var star = this._stars[i];
      star.style.scale = 0.0;
      
      if (star.style.visible) {                
        animateStar(star, i, panel);
      }
    }        
  };
  
  var animateStar = function(star, index, panel) {
    animate(star)
      .wait(1300 + index * 400)
      .then(function() {
        var x = star.style.x + panel.style.x + star.style.width * 0.5;
        var y = star.style.y + panel.style.y + star.style.height * 0.5;

        Sound.play("gmae");
        Effects.flashRayUIStar(x, y);
      })
      .then({scale: 1.2}, 250, animate.easeIn)
      .then({scale: 1.0}, 250, animate.easeOut);
  };  
});
