import src.Utils.Tools as Tools;

/**
* Polar Class
*
* Provide functions to work with polar coordinates
*/
exports = Class(function () {
  this.init = function(origin, radius) {
    this._origin = origin;
    this._scale = 1.0;
    this._radius = radius;
    this.rotation = 0.0;
  };
  
  this.getCoord = function(angle) {
    var theta = angle + this.rotation;
    var res = {};
    res.x = (this._origin.x + this._radius * Math.cos(theta)) * this._scale;
    res.y = (this._origin.y + this._radius * Math.sin(theta)) * this._scale;
    return res;
  };
  
  this.getCoordRadiusExtended = function(angle, ext) {
    var theta = angle + this.rotation;
    var res = {};
    res.x = (this._origin.x + (this._radius + ext) * Math.cos(theta)) * this._scale;
    res.y = (this._origin.y + (this._radius + ext) * Math.sin(theta)) * this._scale;
    return res;
  };  
  
  this.getPolar = function(x, y) {
    var dx = x - this._origin.x;
    var dy = y - this._origin.y;
    
    var theta = Math.atan(dy/dx) - this.rotation;
 	if (x >= 0 && y >= 0) {
 		theta = theta;
 	} else if (x < 0 && y >= 0) {
 		theta = Math.PI + theta;
 	} else if (x < 0 && y < 0) {
 		theta = Math.PI + theta;
 	} else if (x > 0 && y < 0) {
 		theta = (Math.PI * 2) + theta;
 	} 
    
    return Tools.normalizeAngle(theta);
  };
  
  this.isLanded = function(position) {
    var dx = position.x - this._origin.x;
    var dy = position.y - this._origin.y;
    var sqrDist = dx * dx + dy * dy;
    var sqrRadius = this._radius * this._radius;
    return sqrDist <= sqrRadius;
  };
});
