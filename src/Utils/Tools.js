/**
 * Pad a number string with zero
 * @param {Number} width 
 * @param {Number} number
 * @return {String} zero-padded string of the number
 */
exports.zeroPad = function(width, number) 
{
    width -= number.toString().length;
    if (width > 0) return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
    return number + '';
};

/**
 * Return a random number in range inclusively
 * @param {Number} min
 * @param {Number} max
 * @return {Number} random number in range [min, max]
 */
exports.randomRange = function(min, max)
{
  return Math.min(Math.random() * (max - min + 1) + min, max);
};

/**
 * Return a random integer in range inclusively
 * @param {Number} min
 * @param {Number} max
 * @return {Number} random integer in range [min, max]
 */
exports.randomIntRange = function(min, max)
{
  return Math.floor(exports.randomRange(min, max));
};

/**
 * Return the shortest angle between two angle
 * @param {Number} start
 * @param {Number} end
 * @return {Number} shortest angle between start and end
 */
exports.getShortestAngle = function(start, end)
{
    return Math.atan2(Math.sin(end-start), Math.cos(end-start));
};

/**
 * Return the normalized angle 
 * @param {Number} angle
 * @return {Number} normalized angle in radian [0..360]
 */
exports.normalizeAngle = function(angle) {
  angle = angle % (Math.PI * 2);

  if (angle < 0) {
      angle += Math.PI * 2;
  }
  
  return angle;
};

/**
 * Return a randomly selected item from an array.
 * @param {Array} a
 * @return {Object} an item from the array.
 */
exports.randomChoose = function(a) {
  var n = Math.floor(Math.random() * a.length);
  return a[Math.min(n, a.length - 1)];
};
