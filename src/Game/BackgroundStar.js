import animate;
import ui.View;
import ui.ImageScaleView;
import ui.ImageView as ImageView;
import ui.TextView as TextView;
import ui.widget.ButtonView as ButtonView;

import src.Game.Sound as Sound;
import src.Game.Effects as Effects;
import src.Text as Text;

exports = Class(ImageView, function(supr) {
    
  this.init = function(opts)
  { 
    var randomScale = Math.random();
    opts = merge(opts, {
      width: 15 + 14 * randomScale,
      height: 20 + 19 * randomScale,
      image: "resources/images/planet/bg_star.png",
    });
    
    supr(this, 'init', [opts]);                    
    
    var glow = new ImageView({
      superview: this,
      x: 0.5 + 0.5 * randomScale,
      y: 3 + 3 * randomScale,
      anchorX: 13 + 12 * randomScale,
      anchorY: 13 + 12 * randomScale,
      width: 16 + 16 * randomScale,
      height: 16 + 16 * randomScale,
      opacity: 0.3,
      image: "resources/images/planet/star_glow.png",
      compositeOperation: "lighter",
    });
    
    this._originY = opts.y;
    this._floatSpeed = Math.random() * 200 + 600;
    
    animateFloatingStar.call(this);    
    animateGlow.call(glow);
  };  
});

function animateFloatingStar()
{
  var oy = this._originY;
  var tm = this._floatSpeed;
  
  animate(this).clear()
    .now({y: oy - 4}, tm, animate.easeOut)
    .then({y: oy}, tm, animate.easeIn)
    .then(animateFloatingStar.bind(this));        
}

function animateGlow() {
  var randomInterval = Math.random();
  animate(this).clear()
    .now({scale: 1.05 + 0.2 * Math.random(), opacity: 0.7}, 300 + 100 * randomInterval, animate.easeOut)
    .then({scale: 1.0, opacity: 0.3}, 300 + 100 * randomInterval, animate.easeIn)
    .then(animateGlow.bind(this));         
}
