import animate;
import ui.View;
import ui.ImageView as ImageView;
import ui.ViewPool as ViewPool;

import src.Utils.Polar as Polar;
import src.Utils.Tools as Tools;
import src.Game.Building as Building;
import src.Config as Config;

/**
* BuildingManager Class
*
* The manager of buildings.
*/
exports = Class(ui.View, function(supr) {
  this.init = function(opts) {          
    supr(this, 'init', [opts]); 
    
    this._planet = opts.planet;
    this._config = opts.config;
    
    this._pool =  new ViewPool({
      ctor: Building,
      initCount: 100,
      initOpts: {
          superview: this,
      }
    });
    
    this._settledBuildings = [];
    this._gameState = opts.superview;
    this._isDropping = false;
    this._checkingBuildings = [];
    this._level = null;
  };
  
  this.cleanUp = function() {    
    this._isDropping = false;
    this._checkingBuildings.length = 0;
    this._settledBuildings.length = 0;    
    this._currentBuilding = null;
    
    this._pool.releaseAllViews();
  };
  
  this.setLevel = function(level) {
    this._level = level;
    
    var totalValue = 0;
    var droppings = this._level.droppings;    
    for (var name in droppings) {      
      totalValue += droppings[name];
    }
    this._totalDroppingWeights = totalValue;
  };
  
  this.selectRandomBuilding = function() {
    var dice = Math.random() * this._totalDroppingWeights;
    
    var droppings = this._level.droppings;    
    var acc = 0;
    for (var name in droppings) {
      acc += droppings[name];
      
      if (dice < acc) {
        return name;
      }
    }
    
    for (var name in droppings) {
      return name;      
    }
    
    return "plant";
  };
  
  this.createNextBuilding = function() {    
    // TODO - fix hard code
    var bldName = this.selectRandomBuilding();
    if (bldName != null)
    {
      var cfg = Config.Buildings[bldName];
      var building = this._pool.obtainView();
      building.onAppear({
        name: bldName,
        x: Config.General.buildingFloatingPosition.x,
        y: 0,
        toY: Config.General.buildingFloatingPosition.y,
        floatDistance: Config.General.buildingFloatingPosition.floatDistance,
        config: cfg,
      });

      this._currentBuilding = building;
    }
  };
  
  this.dropCurrentBuilding = function() {
    if (this._currentBuilding != null) {
      
      if (this._currentBuilding.special == "pull") {        
        var bld = this._currentBuilding;
        this._currentBuilding = null;
        bld.pull();
      }
      else {
        this._currentBuilding.fall();
        this._settledBuildings.push(this._currentBuilding);

        this._currentBuilding = null;
        this.createNextBuilding();
      }
    }
  };    
  
  this.checkMatch3Potential = function(buildingName, polarPosition) {
    var matchCount = 0;
    for (var i = this._settledBuildings.length - 1; i >= 0; --i) {      
      var bld = this._settledBuildings[i];            
      
      if (bld.isMatchingTo(buildingName, polarPosition)) {
        ++matchCount;
      }
    }
    
    return matchCount >= 2;
  };
  
  this.sortSettledBuildings = function() {
    var a = this._settledBuildings;
    var swapped;
    do {
      swapped = false;
      for (var i=0; i < a.length-1; i++) {               
        if (a[i]._polarPosition < a[i+1]._polarPosition) {
            var temp = a[i];
            a[i] = a[i+1];
            a[i+1] = temp;
            swapped = true;
        }
      }
    } while (swapped);
    
    /*for (var i=0; i < a.length; i++) {              
      a[i]._debugText.setText(i.toString());
    }*/
  };
  
  this.updateBuildings = function(dt) {
    var len = this._settledBuildings.length;
    for (var i = len - 1; i >= 0; --i) {      
      var bld = this._settledBuildings[i];                  
      
      // Delete fused ones
      if (!bld.update(dt)) {
        this._settledBuildings[i] = this._settledBuildings[len - 1];
        this._settledBuildings.length -= 1;
        len = this._settledBuildings.length;
        
        this._pool.releaseView(bld);        
      }
    }
    
    for (var i = len - 1; i >= 0; --i) {      
      var bld = this._settledBuildings[i];                  
      
      if (bld.isFalling() && bld.checkForCollisions()) {
        break;
      }
    }
  };
  
  this.checkBuildingsMatch3 = function() {
    if (this._checkingBuildings.length > 0) {      
      var bld = this._checkingBuildings[0];                
      this._checkingBuildings[0] = this._checkingBuildings[this._checkingBuildings.length - 1];      
      
      // Sort buildings by position
      this.sortSettledBuildings();        
      this.checkMatch3(bld);                               
      
      this._checkingBuildings.length -= 1;
    }
  };
  
  this.processDroppingBuilding = function() {
    if (this._isDropping) {      
      var canDrop = true;
      var len = this._settledBuildings.length;
      for (var i = len - 1; i >= 0; --i) {      
        var bld = this._settledBuildings[i];    
        if (!bld.isSettled() && !bld.isFusing() && !bld.isFlying()) {
          canDrop = false; 
        }
      }
      
      if (canDrop) {
        this.dropCurrentBuilding();        
        this._isDropping = false;
      }
    }
  };
  
  this.update = function(dt) {        
    
    this.sortSettledBuildings();
    
    // Update buildings
    this.updateBuildings(dt);
    
    // Check for collisions and matching in order
    this.checkBuildingsMatch3();
    
    // Make sure the dropping of buildings is in order
    this.processDroppingBuilding();
  };
  
  this.checkPullLineHit = function(bld) {    
    var losWidth = bld.collisionSize;
    var losX1 = bld.style.x - losWidth * 0.5;
    var losX2 = losX1 + losWidth;
    var planetCenterY = this._planet._planetCenter.y;
    
    // Check line of sight collision with others for pulling
    for (var j = this._settledBuildings.length - 1; j >= 0; --j) {
     var otherBld = this._settledBuildings[j];
     if (bld != otherBld && (otherBld.isSettled())) {
       var sz = otherBld.collisionSize;
       var x1 = otherBld.style.x - sz * 0.5;
       var x2 = x1 + sz;
       
       if ((otherBld.style.y < planetCenterY) && (!(losX2 < x1 || losX1 > x2))) {
         return otherBld;
       }
     }
    }
    
    return null;
  };
  
  this.checkCollisions = function(bld) {                
    // Check collision with others
    for (var j = this._settledBuildings.length - 1; j >= 0; --j) {
     var otherBld = this._settledBuildings[j];
     if (bld != otherBld && (otherBld.isSettled() || otherBld.isFlying())) {
       if (bld.isCollidedWith(otherBld)) {
         return otherBld;
       }
     }
    }
    
    return null;
  };
  
  this.checkMatch3 = function(bld) {        
    if (bld.canEvolve()) {
      var idx = this._settledBuildings.indexOf(bld);
      if (idx < 0) {
        return;
      }
      
      // Check matching with others
      var len = this._settledBuildings.length;
      var matchesList = [];
      var n = idx + 1;
      var checker = bld;
      for (var i = len - 2; i >= 0; --i) {
        if (n >= len) {
          n = 0;
        }
        
        var otherBld = this._settledBuildings[n]; 
        ++n;
        
        if (!otherBld.isSettled()) {
          continue;
        }
        
        if (otherBld.isMatchingWith(checker)) {
          checker = otherBld;
          matchesList.push(otherBld);
        }
        else {
          break;
        }
      }
      
      n = idx - 1;
      checker = bld;
      for (var i = len - 2; i >= 0; --i) {
        if (n < 0) {
          n = len - 1;
        }
        
        var otherBld = this._settledBuildings[n]; 
        --n;
        
        if (!otherBld.isSettled()) {
          continue;
        }
        
        if (otherBld.isMatchingWith(checker)) {
          checker = otherBld;
          
          if (matchesList.indexOf(otherBld) < 0) {
            matchesList.push(otherBld);
          }
        }
        else {
          break;
        }
      }

      // Collapse this match
      if (matchesList.length >= 2) {        

        for (var j = matchesList.length - 1; j >= 0; --j) {
          var otherBld = matchesList[j];
          otherBld.fuseIn(bld);
        }

        bld.evolve();
      }    
    }
  };
  
  this.removeBuildingFromSettled = function(building) {
    var len = this._settledBuildings.length;
    for (var i = len - 1; i >= 0; --i) {      
      var bld = this._settledBuildings[i];    
      if (bld == building) {
        this._settledBuildings[i] = this._settledBuildings[len - 1];
        this._settledBuildings.length -= 1;
        break;
      }
    }
  }
  
  this.removeBuilding = function(building, time) {
    this.removeBuildingFromSettled(building);
    
    var pool = this._pool;
    building.style.visible = false;
    if (time > 0) {
      animate(building)
        .clear()
        .wait(time)
        .then(function() {
          pool.releaseView(building);
        });      
    } else {
      animate(building).clear();
      pool.releaseView(building);
    }
  };
  
  
});
