var timerFontData = null;
var scoreFontData = null;

exports.getTimerFontData = function() {
  
  if (timerFontData == null) {
    var d = {};
	for (var i = 0; i < 10; i++) {
		d[i] = {
			image: "resources/images/numbers/" + "timer_" + i + ".png"
		};
	}
	return d;
  }
  
  return timerFontData;
};

exports.getScoreFontData = function() {
  if (scoreFontData == null) {
    var d = {};
	for (var i = 0; i < 10; i++) {
		d[i] = {
			image: "resources/images/numbers/" + "score_" + i + ".png"
		};
	}
    d[","] = {
        image: "resources/images/numbers/" + "score_comma.png"
    };
	return d;
  }
  
  return scoreFontData;
};
