import animate;
import ui.View;
import ui.TextView as TextView;
import ui.ImageView as ImageView;

import src.Game.Sound as Sound;
import src.Game.Effects as Effects;
import src.Utils.Polar as Polar;
import src.Utils.Tools as Tools;
import src.Config as Config;

BuildingState = {
  Floating: 0,
  Falling: 1,
  Settled: 2,
  Fusing: 3,
  Flying: 4,
  Pulling: 5,
};

var BUILDING_GROUNDED_SOUNDS = [
  "stoneHit1",
  "stoneHit2",
  "stoneHit3",
  ];

/**
* BuildingDropper Class
*
* The dropping manager of buildings.
*/
exports = Class(ImageView, function(supr) {
  this.init = function(opts) {              
    supr(this, 'init', [opts]);     
    
    /*this._debugText = new TextView({
      superview: this,
      layout: 'box',
      text: "1",
      zIndex: 100,
      visible: false,
    });*/
    
    this._manager = opts.superview;
    this._polarPosition = 0.0;
  };
  
  this.updateConfig = function(opts) {
    var config = opts.config;
    var w = config.width;
    var h = config.height;    
    var images = config.image;
    var imageUrl = config.imagePath + "/" + images[Tools.randomIntRange(0, images.length - 1)];    
    
    opts = merge(opts, {
      width: w, 
      height: h,
      offsetX: -w / 2,
      offsetY: -h + (h / 16),
      anchorX: w / 2,
      anchorY: h,
      zIndex: 10,
      scale: 1.0,
      r: 0.0,
      image: imageUrl,
      visible: true,
    });
    
    this._fuseInTarget = null;
    this.evolveTo = config.evolve_to;
    this._name = opts.name;    
    this.collisionSize = config.collisionSize;
    this.special = config.special;
    this.updateOpts(opts); 
    
    this._isEvolving = false;
    
    /*this._debugText.updateOpts({
      superview: this,
      layout: 'box',
      scale: 1.0,
      x: 0,
      y: 0,
      width: 50,
      height: 50,
      text: "1",
      zIndex: 100,
      visible: true,
    });*/
  };
  
  this.onAppear = function(opts) {         
    animate(this).clear();
    
    this.updateConfig(opts);
    
    this._state = BuildingState.Floating;

    var view = this;
    animate(this).clear()
      .now({y: opts.toY}, 500, animate.easeOut)
      .then(function() {
        animateFloating.call(view, opts.toY - opts.floatDistance, opts.toY + opts.floatDistance * 2.0);    
      });
  };
  
  this.onPreBuild = function(opts) {    
    animate(this).clear();
    
    this.updateConfig(opts);
    
    var planet = this._manager._planet;
    this._polarPosition = opts.polarPosition;
    this._state = BuildingState.Settled;

    var pos = planet._polar.getCoord(this._polarPosition);
    this.style.x = pos.x;
    this.style.y = pos.y;
    this.style.r = this._polarPosition + planet._polar.rotation + Math.PI / 2;  
  };
  
  this.pull = function() {    
    this._state = BuildingState.Pulling;
    
    var retreatY = -this.style.height * 0.5;
    var manager = this._manager;
    var planet = manager._planet;
    
    var pulledBld = manager.checkPullLineHit(this);
    
    if (pulledBld != null) {
      pulledBld.backToFloating();
      manager._currentBuilding = pulledBld;
    } else {
      manager.createNextBuilding();
    }    
    
    Effects.skyray(this.style.x, -128.0, planet._planetCenter.y - planet._polar._radius);
    
    animate(this).clear()
      .now({y: retreatY}, 500, animate.linear)
      .then(function() {
        manager.removeBuilding(this, 0);          
      });
  };
  
  this.backToFloating = function() {
    var manager = this._manager;
    manager.removeBuildingFromSettled(this);
    
    var self = this;
    var toY = Config.General.buildingFloatingPosition.y;
    animate(this).clear()
      .now({x: Config.General.buildingFloatingPosition.x, y: toY, r: 0.0}, 500, animate.easeOut)
      .then(function () {
        animateFloating.call(self, toY - 10, toY + 10 * 2.0);    
      });
    
    this._state = BuildingState.Floating;
  };
  
  this.fall = function() {    
    var manager = this._manager;
    this._state = BuildingState.Falling;
        
    var planet = manager._planet;    
    var planetCenter = planet.getCenterPosition();
    var landingY = planetCenter.y - planet.getRadius() + 10;
    animate(this).clear()
      .now({y: landingY}, 250, animate.easeIn)
      .then(function() {     
        this._polarPosition = planet._polar.getPolar(this.style.x, this.style.y);      

        if (!this.checkForCollisions()) {
          if (this.special == "demolish") {
            this.style.y = landingY;
            Effects.explode3(this, 0, -this.style.height * 0.5);

            Sound.play("rumble", false);             
            this._manager.removeBuilding(this, 2000);
          }
          else if (this.special == "upgrade") {
            this.style.y = landingY;
            Effects.smokePuff(this);

            Sound.play("rumble", false);
            this._manager.removeBuilding(this, 2000);
          }
          else {            
            Effects.smokePuff(this);
            Sound.play(Tools.randomChoose(BUILDING_GROUNDED_SOUNDS), false);
            this._manager._gameState.addScore(this.canEvolve() ? Config.General.pointsPerDrop : Config.General.pointsPerBigDrop);            
          }
        }                

        this._state = BuildingState.Settled; 
        this._manager._checkingBuildings.push(this);                 
      });
  };
  
  this.checkForCollisions = function() {
    if (this.special == "pull") {
      return false;
    }
    
    var collidedBld = this._manager.checkCollisions(this);
    if (collidedBld != null) {

      if (this.special == "upgrade") {
        this._manager.removeBuilding(this, 0);            
        collidedBld.evolve();
      }
      else {            
        Sound.play("rumble", false);          
        this._manager.removeBuilding(this, 0);
        this._manager.removeBuilding(collidedBld, 0);

        if (this.special != "demolish") {
          Effects.explode3(this, 0, -this.style.height * 0.5);
          Effects.explode1(collidedBld, 0, -collidedBld.style.height * 0.5);
          this._manager._gameState.emit('Lose');              
        }
        else {
          Effects.explode3(collidedBld, 0, -collidedBld.style.height * 0.5);
        }
      }
      
      return true;
    }
    
    return false;
  };
    
  this.update = function(dt) {    
    var manager = this._manager;    
    if (this._state == BuildingState.Settled || this._state == BuildingState.Fusing || this._state == BuildingState.Flying) {      
      var planet = manager._planet;    
      var pos = planet._polar.getCoord(this._polarPosition);
      this.style.x = pos.x;
      this.style.y = pos.y;
      this.style.r = this._polarPosition + planet._polar.rotation + Math.PI / 2;
      
      if (this._state == BuildingState.Fusing) {
        var target = this._fuseInTarget;
        if (target != null) {
          var planet = manager._planet;    

          this._polarPosition += Tools.getShortestAngle(this._polarPosition, target._polarPosition) * dt * 0.008;        
          this.style.scale += -this.style.scale * dt * 0.001;
          if (this.style.scale <= 0.2) {
            this.style.visible = false;
          }
          
          return (this.style.scale > 0.2);
        }
        else
        {
          return false;
        }
      }
      else if (!this.isEvolving() && this._state != BuildingState.Flying) {
        if (this.special == "flying") {
          this.flyAway();
        }
      }      
    }            
    
    return true;
  };
  
  this.isFlying = function() {
    return this._state == BuildingState.Flying;
  };
  
  this.isFusing = function() {
    return this._state == BuildingState.Fusing;
  };
  
  this.isFloating = function() {
    return this._state == BuildingState.Floating;
  };
  
  this.isFalling = function() {
    return this._state == BuildingState.Falling;
  };
  
  this.isSettled = function() {
    return this._state == BuildingState.Settled;
  };
  
  this.isEvolving = function() {
    return this._isEvolving;
  }
  
  this.isCollidedWith = function(other) {
    var dx = this.style.x - other.style.x;
    var dy = this.style.y - other.style.y;    
    var r1 = this.collisionSize * 0.5;
    var r2 = other.collisionSize * 0.5;
    var r = r1 + r2;
    return (dx * dx + dy * dy) <= r * r;
  };
  
  this.isMatchingTo = function(name, polarPosition) {
    if (name == this._name) {            
      var alpha = Tools.getShortestAngle(this._polarPosition, polarPosition);      
      return Math.abs(alpha) <= Config.General.leastAngleForMatching;
      return true;
    }    
    return false;
  };
  
  this.isMatchingWith = function(other) {
    return this.isMatchingTo(other._name, other._polarPosition);
  };
  
  this.isMatchingWithList = function(list) {
    for (var i = list.length - 1; i >= 0; --i) {
      var bld = list[i];
      if (this.isMatchingWith(bld)) {
        return true;
      }
    }
    
    return false;
  };
  
  this.canEvolve = function() {
    return this.evolveTo != null;
  };
  
  this.evolve = function() {    
    this._isEvolving = true;
    
    Effects.flashRay(this, 0, -this.style.height * 0.4);
    Sound.play("completetask_0", false);
    
    this._manager._gameState.addScore(Config.General.pointsPerEvolve);
    
    animate(this).clear()
      .now({scale: 0.25}, 200, animate.easeIn)
      .then(function() {
        var bldName = this.evolveTo;
        if (bldName != null) {
          this.updateConfig({
            name: bldName,
            config: Config.Buildings[bldName],
          });
          
          this._manager._checkingBuildings.push(this);        
        }        
      })
      .then({scale: 1.3}, 200, animate.easeIn)
      .then({scale: 1.0}, 200, animate.easeIn)
      .then(function() {        
        this._isEvolving = false;
      });
  };
  
  this.fuseIn = function(target) {
    this._manager._gameState.addScore(Config.General.pointsPerFusion);
    
    this._state = BuildingState.Fusing;
    this._fuseInTarget = target;
    this.style.zIndex = 1;
  };

  this.flyAway = function() {
    this._manager._gameState.addScore(Config.General.pointsPerEscape);
    this._state = BuildingState.Flying;    
    
    var self = this;
    var manager = this._manager;
    var planet = manager._planet;    
    var awayPos;
    animate(this).clear()
      .wait(500)
      .now({scale: 0.8}, 100, animate.easeIn)
      .then({scale: 1.1}, 100, animate.easeIn)
      .then({scale: 0.8}, 100, animate.easeIn)
      .then({scale: 1.1}, 100, animate.easeIn)
      .then({scale: 0.8}, 100, animate.easeIn)
      .then({scale: 1.1}, 100, animate.easeIn)      
      .then(function() {
        Effects.fireworks(this, (Math.random() * 1.0 - 0.5) * this.style.width * 0.5, -this.style.height * 0.5);
      })
      .then({scale: 0.25}, 300, animate.easeIn)
      .then(function() {
        Effects.fireworks(this, (Math.random() * 1.0 - 0.5) * this.style.width * 0.5, -this.style.height * 0.5);
      })
      .then({scale: 0.6}, 100, animate.linear)
      .then(function() {        
        Effects.explode2(this, 0, -this.style.height * 0.25);
      })
      .then({scale: 1.0}, 100, animate.easeIn)
      .then(function() {
        Effects.fireworks(this, (Math.random() * 1.0 - 0.5) * this.style.width * 0.5, -this.style.height * 0.5);
        awayPos = planet._polar.getCoordRadiusExtended(this._polarPosition, planet._planetCenter.y);        
        Sound.play("completetask_0", false);
        animate(this).clear()
          .now({x: awayPos.x, y: awayPos.y}, 1000, animate.easeOut)
          .then(function() {
            manager.removeBuilding(self);
          });
      });
  };
});

function animateFloating(topY, botY)
{    
  animate(this)
    .now({y: botY}, 500, animate.easeOut)
    .then({y: topY}, 500, animate.easeIn)
    .then(animateFloating.bind(this, topY, botY));
}
    