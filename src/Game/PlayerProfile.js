import src.Utils.Data as Data;

var prefsKey = "prefs";

var playerPrefs = {  
  hasMusic: true,
  hasSound: true,
  highscore: 0,
};

exports.load = function() {
  if (Data.has(prefsKey)) {
    playerPrefs = Data.get(prefsKey);
  }
};

exports.save = function() {
  Data.set(prefsKey, playerPrefs);
};

exports.announceScore = function(score) {
  if (playerPrefs.highscore == null) {
    playerPrefs.highscore = score;
  }
  else if (playerPrefs.highscore < score) {
    playerPrefs.highscore = score;
  }
};

exports.getBestScore = function() {
  return playerPrefs.highscore != null ? playerPrefs.highscore : 0;
}
