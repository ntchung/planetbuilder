import AudioManager;
import src.Utils.Tools as Tools;

var soundManager = null;
var lastPlayedMusic = null;
var currentMusicIndex = 0;

var MUSIC_SOUNDS = [
  "polka_train",
  "alpha_dance",
  "drumming_sticks",
  "farm_frolics",
  "italian_mom",
  "time_driving",
  ];

this.init = function() {
  if (soundManager != null) {
    return;
  }
  
  soundManager = new AudioManager({
    path: "resources/sounds/",
    files: {
      polka_train: {
        volume: 0.7,
        loop: false,
        background: true
      },
      alpha_dance: {
        volume: 0.7,
        loop: false,
        background: true
      },      
      drumming_sticks: {
        volume: 0.7,
        loop: false,
        background: true
      },      
      farm_frolics: {
        volume: 0.7,
        loop: false,
        background: true
      },      
      italian_mom: {
        volume: 0.7,
        loop: false,
        background: true
      },
      time_driving: {
        volume: 0.7,
        loop: false,
        background: true
      },
      stoneHit1: {
        volume: 1.0,
      },
      stoneHit2: {
        volume: 1.0,
      },
      stoneHit3: {
        volume: 1.0,
      },
      rumble: {
        volume: 1.0,
      },
      completetask_0: {
        volume: 1.0,
      },
      losing: {
        volume: 0.9,
      },
      gmae: {
        volume: 1.0,
      },
      firework: {
        volume: 0.7,
      },
    }
  });
  
  currentMusicIndex = Math.floor(Math.random() * MUSIC_SOUNDS.length);
};

this.play = function(name, isLoop) {
  soundManager.play(name, {loop: isLoop});  
};

this.playRandomMusic = function() {
  soundManager.stop(MUSIC_SOUNDS[currentMusicIndex]);
  
  ++currentMusicIndex;
  if (currentMusicIndex >= MUSIC_SOUNDS.length) {
    currentMusicIndex = 0;
  }
    
  soundManager.play(MUSIC_SOUNDS[currentMusicIndex], {loop: false});  
};

