import ui.View;
import ui.ImageView as ImageView;

import src.Utils.Polar as Polar;
import src.Utils.Tools as Tools;
import src.Game.Effects as Effects;
import src.Game.BackgroundStar as BackgroundStar;

/**
* Planet Class
*
* The rotating planet with background and decorations.
*/
exports = Class(ui.View, function(supr) {
  this.init = function(opts) {          
    supr(this, 'init', [opts]); 
    
    // Initial values
    this._config = opts.config;
    this._rotateSpeed = 1.0;
    this._currentRotateSpeed = 0.0;
    this._lapsBeforeReverse = 0;
    this._acceleration = 1.0;
    
    this._initialRotation = 0.0;
    this._rotatedAngleCount = 0;
    this._lapsCount = 0;
    
    var planetPosition = this._config.position;
    var bkgSize = this._config.backgroundSize;
    var planetSize = this._config.size;    
    var terrainSize = this._config.terrainSize;
    
    var bottom = GLOBAL.screenHeight + 10;
    this._planetCenter = {
      x: (GLOBAL.screenWidth) / 2,
      y: bottom - (bkgSize.height - planetPosition.y),
    };
    
    // space background
    var background = new ImageView({
      superview: this,
      offsetX: -(bkgSize.width * 0.5),
      offsetY: -bkgSize.height,
      x: GLOBAL.screenWidth / 2,
      y: bottom,      
      width: bkgSize.width,
      height: bkgSize.height,         
      image: opts.config.backgroundImage,
    });            
    
    // stars
    this._stars = [];
    this.generateStars();
    
    
    // halo
    var haloSize = terrainSize.width * 1.48;
    this._halo = new ImageView({
      superview: this,
      width: haloSize,
      height: haloSize,
      anchorX: (haloSize * 0.5),
      anchorY: (haloSize * 0.5),
      x: this._planetCenter.x - (haloSize * 0.5),
      y: this._planetCenter.y - (haloSize * 0.5), 
      image: opts.config.haloImage,
      compositeOperation: 'lighter',
    });
  
    // the planet    
    this._terrain = new ImageView({
      superview: this,
      x: this._planetCenter.x,
      y: this._planetCenter.y,
      offsetX: -(terrainSize.width * 0.5),
      offsetY: -(terrainSize.height * 0.5),
      anchorX: (terrainSize.width * 0.5),
      anchorY: (terrainSize.height * 0.5),      
      width: terrainSize.width,
      height: terrainSize.height,
      image: opts.config.terrainImage,
      r: Math.random() * Math.PI * 2,
    });            
    
    // Polar coordinate
    this._polar = new Polar(this._planetCenter, this._config.planetRadius);    
  };
  
  this.reset = function() {
    this._currentRotateSpeed = 0.0;
  }
  
  this.getRadius = function() {
    return this._config.planetRadius;
  };
  
  this.getCenterPosition = function() {
    return this._planetCenter;
  };
  
  this.setRotateSpeed = function(speed) {
    this._rotateSpeed = Math.random() < 0.5 ? -speed : speed;     
  };
  
  this.setInitialRotation = function(rotation) {
    this.setRotation(rotation);
    
    this._initialRotation = this._polar.rotation;
    this._lapsCount = 0;
    this._rotatedAngleCount = 0;
  };
  
  this.setRotation = function(rotation) {
    this._polar.rotation = Tools.normalizeAngle(rotation);
    this._terrain.style.r = this._polar.rotation;    
    this._halo.style.r = this._polar.rotation;        
  };
  
  this.setReversal = function(laps) {
    this._lapsBeforeReverse = laps != null ? laps : 0;
  };
  
  this.setAcceleration = function(acc) {
    this._acceleration = acc != null ? acc : 1;
  }
  
  this.update = function(dt) {    
    
    this._currentRotateSpeed += (this._rotateSpeed - this._currentRotateSpeed) * dt * 0.001 * this._acceleration;
    
    var d = dt * 0.001 * this._currentRotateSpeed;         
    var newAngle = this._polar.rotation + d;
    this.setRotation(newAngle);            
    this._polar._scale = this.style.scale;    
    
    if (this._lapsBeforeReverse > 0) {
      this._rotatedAngleCount += d;
      var a = Math.abs(this._rotatedAngleCount);
      if (a >= Math.PI * 2) {
        this._rotatedAngleCount = 0;
        ++this._lapsCount;
        
        // Reverse !
        if (this._lapsCount >= this._lapsBeforeReverse) {
          this._lapsCount = 0;
          
          this._rotateSpeed = -this._rotateSpeed;
        }
      }
    }
  };
  
  this.isBuildingLanded = function(building) {
    return this._polar.isLanded({
      x: building.style.x,
      y: building.style.y
    });
  };
  
  this.shake = function(distance, duration) {
    Effects.shake(this, {scale: distance, duration: duration});    
  };
  
  this.generateStars = function() {
    var terrainSize = this._config.terrainSize;
    
    var fromY = this._planetCenter.y - terrainSize.height * 0.5;
    
    var h = 128;
    
    var n = 0;
    var len = this._stars.length;
    while (fromY > 128) {
      var x = Math.random() * (GLOBAL.screenWidth / 2 - 32) + 32;
      var y = Math.random() * h + fromY;                
      if (n >= len) {
        var star = new BackgroundStar({
          superview: this,
          x: x,
          y: y,
        });
        this._stars.push(star);
      }
      else {
        var star = this._stars[n];
        star.style.x = x;
        star.style.y = y;
      }
      
      ++n;
      
      x = GLOBAL.screenWidth - 32 - Math.random() * (GLOBAL.screenWidth / 2 - 32);
      y = Math.random() * h + fromY;          
      if (n >= len) {
        var star = new BackgroundStar({
          superview: this,
          x: x,
          y: y,
        });
        this._stars.push(star);
      }
      else {
        var star = this._stars[n];
        star.style.x = x;
        star.style.y = y;
      }
      
      fromY -= h;
      ++n;
    }
  };
});
